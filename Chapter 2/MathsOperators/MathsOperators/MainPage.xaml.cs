﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MathsOperators
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnCalculateClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((bool)_rbtnAddition.IsChecked)
                {
                    AddValues();
                }
                else if ((bool)_rbtnSubtraction.IsChecked)
                {
                    SubtractValues();
                }
                else if ((bool)_rbtnMultiplication.IsChecked)
                {
                    MultiplyValues();
                }
                else if ((bool)_rbtnIntDivision.IsChecked)
                {
                    IntDivideValues();
                }
                else if ((bool)_rbtnDivision.IsChecked)
                {
                    RealDivideValues();
                }
                else if ((bool)_rbtnRemainder.IsChecked)
                {
                    RemainderValues();
                }
            }
            catch (Exception caught)
            {
                _txtExpression.Text = "";
                _txResult.Text = caught.Message;
            }
        }

        private void AddValues()
        {
            int lhs = int.Parse(_txtLhsOperand.Text);
            int rhs = int.Parse(_txtRhsOperand.Text);
            int outcome = 0;
            
            // TODO: Add rhs to lhs and store the _txResult in outcome

            _txtExpression.Text = $"{_txtLhsOperand.Text} + {_txtRhsOperand.Text}";
            _txResult.Text = outcome.ToString();
        }

        private void SubtractValues()
        {
            int lhs = int.Parse(_txtLhsOperand.Text);
            int rhs = int.Parse(_txtRhsOperand.Text);
            int outcome = 0;

            // TODO: Subtract rhs from lhs and store the _txResult in outcome

            _txtExpression.Text = $"{_txtLhsOperand.Text} - {_txtRhsOperand.Text}";
            _txResult.Text = outcome.ToString();
        }

        private void MultiplyValues()
        {
            int lhs = int.Parse(_txtLhsOperand.Text);
            int rhs = int.Parse(_txtRhsOperand.Text);
            int outcome = 0;
            
            // TODO: Multiply lhs by rhs and store the _txResult in outcome

            _txtExpression.Text = $"{_txtLhsOperand.Text} * {_txtRhsOperand.Text}";
            _txResult.Text = outcome.ToString();
        }

        private void IntDivideValues()
        {
            int lhs = int.Parse(_txtLhsOperand.Text);
            int rhs = int.Parse(_txtRhsOperand.Text);
            int outcome = 0;
            
            // TODO: Divide lhs by rhs and store the _txResult in outcome

            _txtExpression.Text = $"{_txtLhsOperand.Text} / {_txtRhsOperand.Text}";
            _txResult.Text = outcome.ToString();
        }

        private void RealDivideValues()
        {
            //TODO: Use the similar code as the integer division but ensure the variables used
            //are double not int
        }

        private void RemainderValues()
        {
            int lhs = int.Parse(_txtLhsOperand.Text);
            int rhs = int.Parse(_txtRhsOperand.Text);
            int outcome = 0;

            // TODO: Work out the remainder after dividing lhs by rhs and store the _txResult in outcome

            _txtExpression.Text = $"{_txtLhsOperand.Text} % {_txtRhsOperand.Text}";
            _txResult.Text = outcome.ToString();
        }
    }
}
